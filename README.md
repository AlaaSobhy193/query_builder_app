# Query Builder App

Query Bulider app using Provider as state management, using dio to handle restful api.

### To run the application execute the following command

```bash
flutter pub get
flutter run
```

### Project screenshots

<table>
    <tr>
        <td><img src="readme_images/screen_shot1.png"  width=270 height=480></td>
        <td><img src="readme_images/screen_shot2.png"  width=270 height=480></td>
        <td><img src="readme_images/screen_shot3.png"  width=270 height=480></td>
    </tr>
   
 </table>
