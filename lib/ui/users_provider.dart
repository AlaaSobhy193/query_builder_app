import 'package:flutter/material.dart';
import 'package:flutter_task/db/models/query_model.dart';
import 'package:flutter_task/db/models/user_model.dart';
import 'package:flutter_task/db/repositories/user_repository.dart';

class UserProvider extends ChangeNotifier {
  UserRepository userRepository = UserRepository();

  List<UserModel>? _users = [];
  List<QueryModel>? queries = [];

  UserProvider() {
    addItemtoList();
    getUsers();
  }

  void getUsers() {
    userRepository.getUsers().then((result) {
      _users = result;
      notifyListeners();
    });
  }

  get filteredUsers =>
      _users!.where((user) => filterByLogicOperation(user)).toList();

  addItemtoList() {
    final item = QueryModel(
      feild: Feilds.values[1],
      operator: Operations.values[0],
      logicOperation: LogicOperation.values[0],
      search: "",
    );
    queries!.add(item);
    notifyListeners();
  }

  removeItemfromList(int index) {
    queries!.removeAt(index);
    notifyListeners();
  }

  filterByQuery(UserModel? user, QueryModel query) {
    var userValue = user!.getUserValue(query.feild!);
    dynamic result;
    switch (query.operator!) {
      case Operations.startsWith:
        result = userValue.toString().startsWith(query.search!);
        break;
      case Operations.endsWith:
        result = userValue.toString().endsWith(query.search!);
        break;
      case Operations.contains:
        result = userValue.toString().contains(query.search!);
        break;
      case Operations.exact:
        result = userValue == query.search!;
        break;
      case Operations.equal:
        result = userValue == int.parse(query.search!);
        break;
      case Operations.notEqual:
        result = userValue != int.parse(query.search!);
        break;
      case Operations.lessThan:
        result = userValue < int.parse(query.search!);
        break;
      case Operations.greaterThan:
        result = userValue > int.parse(query.search!);
        break;
    }
    return result;
  }

  filterByLogicOperation(UserModel? user) {
    if (queries!.isEmpty) return true;
    var finalResult = filterByQuery(user, queries![0]);

    for (var query in queries!) {
      var filteredResultByQuery = filterByQuery(user, query);
      switch (query.logicOperation!) {
        case LogicOperation.and:
          finalResult = finalResult && filteredResultByQuery;
          break;
        case LogicOperation.or:
          finalResult = finalResult || filteredResultByQuery;

          break;
      }
    }
    return finalResult;
  }
}
