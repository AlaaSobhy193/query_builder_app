import 'package:flutter/material.dart';
import 'package:flutter_task/db/models/query_model.dart';
import 'package:flutter_task/widgets/custom_dropdown.dart';
import 'package:flutter_task/widgets/custom_textfeild.dart';
import 'package:gap/gap.dart';

class QueryBuilderItem extends StatefulWidget {
  final QueryModel? query;
  final Function()? onRemove;
  final bool? isFirstItem;
  final bool? isLastItem;
  const QueryBuilderItem({
    Key? key,
    this.query,
    this.onRemove,
    this.isFirstItem,
    this.isLastItem,
  }) : super(key: key);

  @override
  State<QueryBuilderItem> createState() => _QueryBuilderItemState();
}

class _QueryBuilderItemState extends State<QueryBuilderItem> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).cardColor,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            flex: 5,
                            child: DropdownWidget(
                              items: Feilds.values,
                              hint: "Select Search Feild",
                              value: widget.query!.feild!.toLocaleString(),
                              onChange: (value) {
                                setState(() {
                                  widget.query!.feild =
                                      (value as String?)!.getFeildsEnum();
                                  widget.query!.operator = null;
                                  if (widget.query!.feild != Feilds.userId &&
                                      widget.query!.feild != Feilds.age) {
                                    widget.query!.operator =
                                        Operations.values[0];
                                  } else {
                                    widget.query!.operator =
                                        Operations.values[4];
                                  }
                                });
                              },
                            ),
                          ),
                          const Gap(8),
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              items: (widget.query!.feild != Feilds.userId &&
                                      widget.query!.feild != Feilds.age)
                                  ? Operations.values.getRange(0, 4).toList()
                                  : Operations.values.getRange(4, 8).toList(),
                              hint: "Select",
                              value: widget.query!.operator!.toLocaleString(),
                              onChange: (value) {
                                setState(() {
                                  widget.query!.operator =
                                      (value as String?)!.getOperationsEnum();
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                      const Gap(10),
                      TextFeildWidget(
                        hint: 'Enter',
                        onChanged: (value) {
                          widget.query!.search = value;
                          setState(() {});
                        },
                      )
                    ],
                  ),
                ),
                Visibility(
                  visible: !widget.isFirstItem!,
                  child: InkWell(
                    onTap: widget.onRemove,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Colors.red,
                          ),
                        ),
                        child: const Center(
                          child: Icon(
                            Icons.close,
                            color: Colors.red,
                            size: 22,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Visibility(
          visible: !widget.isLastItem!,
          child: Column(
            children: [
              const Gap(30),
              SizedBox(
                width: 90,
                child: DropdownWidget(
                  items: LogicOperation.values,
                  hint: "Select",
                  value: widget.query!.logicOperation!.toLocaleString(),
                  onChange: (value) {
                    setState(() {
                      widget.query!.logicOperation =
                          (value as String?)!.getLogicOperationEnum();
                    });
                  },
                ),
              ),
              const Gap(30),
            ],
          ),
        ),
      ],
    );
  }
}
