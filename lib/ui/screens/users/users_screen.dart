import 'package:flutter/material.dart';
import 'package:flutter_task/db/models/user_model.dart';
import 'package:flutter_task/ui/screens/users/user_item.dart';
import 'package:gap/gap.dart';

class UsersScreen extends StatefulWidget {
  final List<UserModel> users;
  const UsersScreen({Key? key, required this.users}) : super(key: key);

  @override
  State<UsersScreen> createState() => _UsersScreenState();
}

class _UsersScreenState extends State<UsersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).cardColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () => Navigator.pop(context),
                  child: const Icon(
                    Icons.arrow_back_rounded,
                    size: 28,
                  ),
                ),
                const Gap(24),
                const Text(
                  "Users",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w900,
                  ),
                ),
                const Gap(20),
                widget.users.isEmpty
                    ? Center(
                        child: Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.3),
                        child: Text(
                          "no results".toUpperCase(),
                          style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ))
                    : ListView.builder(
                        itemCount: widget.users.length,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return UserItem(
                            user: widget.users[index],
                          );
                        },
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
