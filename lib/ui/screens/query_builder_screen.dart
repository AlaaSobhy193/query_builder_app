import 'package:flutter/material.dart';
import 'package:flutter_task/ui/screens/query_builder_item.dart';
import 'package:flutter_task/ui/screens/users/users_screen.dart';
import 'package:flutter_task/ui/users_provider.dart';
import 'package:gap/gap.dart';
import 'package:provider/provider.dart';

class QueryBuilderScreen extends StatefulWidget {
  const QueryBuilderScreen({Key? key}) : super(key: key);

  @override
  State<QueryBuilderScreen> createState() => _QueryBuilderScreenState();
}

class _QueryBuilderScreenState extends State<QueryBuilderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ChangeNotifierProvider<UserProvider>(
        create: (BuildContext context) => UserProvider(),
        child: Consumer<UserProvider>(
          builder: (context, userProvider, _) {
            return SafeArea(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(16.0) +
                      const EdgeInsets.only(top: 50),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Query Builder",
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          InkWell(
                            onTap: (() => setState(
                                  () {
                                    userProvider.addItemtoList();
                                  },
                                )),
                            child: Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: const Color(0xff53ab77),
                                ),
                              ),
                              child: const Center(
                                child: Icon(
                                  Icons.add,
                                  color: Color(0xff53ab77),
                                  size: 22,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const Gap(30),
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: userProvider.queries!.length,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (_, int index) {
                            return QueryBuilderItem(
                              isFirstItem: index == 0,
                              isLastItem:
                                  index == userProvider.queries!.length - 1,
                              query: userProvider.queries![index],
                              onRemove: () {
                                userProvider.removeItemfromList(index);
                              },
                            );
                          }),
                      const Gap(30),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => UsersScreen(
                                  users: userProvider.filteredUsers!),
                            ),
                          );
                        },
                        child: Container(
                          height: 55,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: const Center(
                              child: Icon(
                            Icons.search,
                            color: Colors.white,
                            size: 35,
                          )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
