import 'package:flutter/material.dart';
import 'package:flutter_task/db/models/query_model.dart';

class DropdownWidget extends StatefulWidget {
  final List<dynamic>? items;
  final String? hint;
  final Function(dynamic)? onChange;
  final dynamic value;
  const DropdownWidget({
    Key? key,
    this.items,
    this.hint,
    this.onChange,
    this.value,
  }) : super(key: key);

  @override
  State<DropdownWidget> createState() => _DropdownWidgetState();
}

class _DropdownWidgetState extends State<DropdownWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 44,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: Colors.grey.shade300),
          ),
          child: DropdownButton<dynamic>(
            items: widget.items!.map((item) {
              return DropdownMenuItem<dynamic>(
                value: item is Feilds
                    ? item.toLocaleString()
                    : item is LogicOperation
                        ? item.toLocaleString()
                        : item is Operations
                            ? item.toLocaleString()
                            : item,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      item is Feilds
                          ? item.toLocaleString()
                          : item is LogicOperation
                              ? item.toLocaleString()
                              : item is Operations
                                  ? item.toLocaleString()
                                  : item,
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              );
            }).toList(),
            hint: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6),
              child: Text(
                widget.hint!,
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.grey,
                  fontSize: 14,
                ),
              ),
            ),
            isExpanded: true,
            underline: Container(),
            iconEnabledColor: Colors.black,
            iconSize: 26,
            onChanged: widget.onChange,
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            value: widget.value,
          ),
        ),
      ],
    );
  }
}
