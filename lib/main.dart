import 'package:flutter/material.dart';
import 'package:flutter_task/ui/screens/query_builder_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Query Builder App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: const Color(0xff2C9CDB),
        fontFamily: 'Poppins',
        cardColor: const Color(0xffF1F1F1),
      ),
      home: const QueryBuilderScreen(),
    );
  }
}
