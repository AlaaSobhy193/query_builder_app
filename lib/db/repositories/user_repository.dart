import 'package:flutter_task/db/models/user_model.dart';
import 'package:flutter_task/services/user_service.dart';

class UserRepository {
  final UserService _userService = UserService();
  Future<List<UserModel>> getUsers() {
    return _userService.getUsers();
  }
}
