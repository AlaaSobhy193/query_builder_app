import 'package:flutter_task/db/models/query_model.dart';

class UserModel {
  int? userId;
  String? firstName;
  String? lastName;
  String? fullName;
  String? gender;
  int? age;

  UserModel({
    this.userId,
    this.firstName,
    this.lastName,
    this.fullName,
    this.gender,
    this.age,
  });

  UserModel.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    fullName = json['full_name'];
    gender = json['gender'];
    age = json['age'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (userId != null) data['user_id'] = userId;
    if (firstName != null) data['first_name'] = firstName;
    if (lastName != null) data['last_name'] = lastName;
    if (fullName != null) data['full_name'] = fullName;
    if (gender != null) data['gender'] = gender;
    if (age != null) data['age'] = age;
    return data;
  }

  getUserValue(Feilds feild) {
    switch (feild) {
      case Feilds.userId:
        return userId;

      case Feilds.firstName:
        return firstName;

      case Feilds.lastName:
        return lastName;

      case Feilds.fullName:
        return fullName;

      case Feilds.gender:
        return gender;

      case Feilds.age:
        return age;
    }
  }
}
