class QueryModel {
  Feilds? feild;
  Operations? operator;
  String? search;
  LogicOperation? logicOperation;
  QueryModel({
    this.feild,
    this.operator,
    this.search,
    this.logicOperation,
  });
}

enum Operations {
  startsWith,
  endsWith,
  contains,
  exact,
  equal,
  notEqual,
  lessThan,
  greaterThan,
}

extension OperationsExtensions on Operations {
  toLocaleString() {
    switch (this) {
      case Operations.startsWith:
        return "Starts With";
      case Operations.endsWith:
        return "Ends With";
      case Operations.contains:
        return "Contains";
      case Operations.exact:
        return "Exact";
      case Operations.equal:
        return "=";
      case Operations.notEqual:
        return "!=";
      case Operations.lessThan:
        return "<";
      case Operations.greaterThan:
        return ">";
    }
  }
}

extension GetOperationsExtensions on String {
  getOperationsEnum() {
    switch (this) {
      case "Starts With":
        return Operations.startsWith;
      case "Ends With":
        return Operations.endsWith;
      case "Contains":
        return Operations.contains;
      case "Exact":
        return Operations.exact;
      case "=":
        return Operations.equal;
      case "!=":
        return Operations.notEqual;
      case "<":
        return Operations.lessThan;
      case ">":
        return Operations.greaterThan;
    }
  }
}

enum LogicOperation {
  and,
  or,
}

extension LogicOperationExtensions on LogicOperation {
  toLocaleString() {
    switch (this) {
      case LogicOperation.and:
        return "AND";
      case LogicOperation.or:
        return "OR";
    }
  }
}

extension GetLogicOperationExtensions on String {
  getLogicOperationEnum() {
    switch (this) {
      case "AND":
        return LogicOperation.and;
      case "OR":
        return LogicOperation.or;
    }
  }
}

enum Feilds {
  userId,
  firstName,
  lastName,
  fullName,
  gender,
  age,
}

extension FeildsExtensions on Feilds {
  toLocaleString() {
    switch (this) {
      case Feilds.userId:
        return "User Id";
      case Feilds.firstName:
        return "First Name";
      case Feilds.lastName:
        return "Last Name";
      case Feilds.fullName:
        return "Full Name";
      case Feilds.gender:
        return "Gender";
      case Feilds.age:
        return "Age";
    }
  }
}

extension GetFeildsExtensions on String {
  getFeildsEnum() {
    switch (this) {
      case "User Id":
        return Feilds.userId;
      case "First Name":
        return Feilds.firstName;
      case "Last Name":
        return Feilds.lastName;
      case "Full Name":
        return Feilds.fullName;
      case "Gender":
        return Feilds.gender;
      case "Age":
        return Feilds.age;
    }
  }
}
