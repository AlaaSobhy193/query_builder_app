import 'package:dio/dio.dart';
import 'package:flutter_task/db/models/user_model.dart';

class UserService {
  Future<List<UserModel>> getUsers() async {
    Dio dio = Dio();
    dio.options.connectTimeout = 50000;
    dio.options.receiveTimeout = 30000;
    dio.interceptors.add(LogInterceptor(responseBody: true, requestBody: true));
    return dio
        .get("https://mocki.io/v1/e67ffca5-c48b-4263-a7dd-5fa236ce7843")
        .then((response) {
      return (response.data as List).map((e) => UserModel.fromJson(e)).toList();
    });
  }
}
